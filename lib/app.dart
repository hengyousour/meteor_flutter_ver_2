import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:meteor_flutter_ver_2/providers/connection_provider.dart';
import 'package:meteor_flutter_ver_2/screens/connection.dart';

class MyApp extends StatefulWidget {
  // This widget is the root of your application.

  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  void initState() {
    super.initState();
    context.read<ConnectionProvider>().initConnection();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: Connection(),
    );
  }
}
