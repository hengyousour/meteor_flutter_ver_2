import 'package:enhanced_meteorify/enhanced_meteorify.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:meteor_flutter_ver_2/shared_preferences/connection.dart';

class ConnectionProvider with ChangeNotifier {
  bool _connectionStatus = false;
  bool get connectionStatus => _connectionStatus;
  String? _ip;
  bool get _isConnected => Meteor.isConnected;

  void initConnection({String? ip}) async {
    _ip = await ConnectionStorage().getIpAddress() ?? ip;
    print('ip: $_ip');

    try {
      if (_ip != null) {
        WidgetsFlutterBinding.ensureInitialized();
        if (_isConnected) disconnect();
        await Meteor.connect('ws://$_ip/websocket', enableLogs: false);

        //real time connection
        listenConnection();
      }
    } catch (error) {
      print(error);
      //Handle error
    }
  }

  void listenConnection() {
    // listen real time connection meteor with flutter

    Meteor.connectionListener = (ConnectionStatus connectionStatus) {
      print(connectionStatus);
      print('listen to ip : $_ip');
      if (describeEnum(connectionStatus) == "CONNECTED") {
        _connectionStatus = true;
      } else {
        _connectionStatus = false;
      }
      notifyListeners();
    };
  }

  void setIp({required String id}) async {
    ConnectionStorage().setIpAddress(ip: id);
    disconnect();
    initConnection(ip: id);
  }

  void disconnect() {
    print('start call disconnect method');
    Meteor.disconnect();
  }
}
