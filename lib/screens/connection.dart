import 'package:enhanced_meteorify/enhanced_meteorify.dart';
import 'package:provider/provider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:meteor_flutter_ver_2/providers/connection_provider.dart';

class Connection extends StatelessWidget {
  final _formKey = GlobalKey<FormBuilderState>();

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Center(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              FormBuilder(
                key: _formKey,
                child: Column(
                  children: [
                    Consumer<ConnectionProvider>(
                      builder: (_, state, child) => Text(
                        'Connection Status ${state.connectionStatus}',
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: state.connectionStatus == true
                              ? Colors.greenAccent
                              : Colors.redAccent,
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 10.0,
                    ),
                    FormBuilderTextField(
                      name: 'ipAddress',
                      validator: FormBuilderValidators.compose([
                        FormBuilderValidators.required(context),
                      ]),
                    ),
                    SizedBox(
                      height: 10.0,
                    ),
                    MaterialButton(
                      color: Theme.of(context).accentColor,
                      child: Text(
                        "Connect To Meteor",
                        style: TextStyle(color: Colors.white),
                      ),
                      onPressed: () {
                        if (_formKey.currentState!.saveAndValidate()) {
                          String _ipAddressUserInput =
                              _formKey.currentState!.value['ipAddress'];
                          context
                              .read<ConnectionProvider>()
                              .setIp(id: _ipAddressUserInput);
                        } else {
                          print("validation failed");
                        }
                      },
                    ),
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
