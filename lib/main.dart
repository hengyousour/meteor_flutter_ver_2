import 'package:enhanced_meteorify/enhanced_meteorify.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'app.dart';
import 'providers/connection_provider.dart';

void main() {
  runApp(
    MultiProvider(
      providers: [
        ChangeNotifierProvider(
          create: (ctx) => ConnectionProvider(),
        ),
      ],
      child: MyApp(),
    ),
  );
}

// void main() async {
//   try {
//     WidgetsFlutterBinding.ensureInitialized();
//     final ip = await PrefsService.getString('ip');
//     ConnectionService connection = ConnectionService();
//     if (ip.isNotEmpty) {
//       connection.setIP(ip);
//       final status = await connection.connect();
//       // print('status : $status');
//     }

//     runApp(MyApp());
//   } on MeteorError catch (error) {
//     print(error.message);
//   }
// }

// class MyApp extends StatelessWidget {
//   // This widget is the root of your application.
//   @override
//   Widget build(BuildContext context) {
//     return MaterialApp(
//       title: 'Flutter Demo',
//       theme: ThemeData(
//         primarySwatch: Colors.blue,
//       ),
//       home: MyHomePage(title: 'Flutter Demo Home Page'),
//     );
//   }
// }

// class MyHomePage extends StatefulWidget {
//   MyHomePage({Key? key, required this.title}) : super(key: key);

//   final String title;

//   @override
//   _MyHomePageState createState() => _MyHomePageState();
// }

// class _MyHomePageState extends State<MyHomePage> {
//   final _controller = TextEditingController();

//   void _connect() {
//     if (_controller.text.isNotEmpty) {
//       PrefsService.setString('ip', _controller.text);
//       ConnectionService conn = ConnectionService();
//       conn.setIP(_controller.text);
//       conn.connect();
//     }
//   }

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(
//         title: Text(widget.title),
//       ),
//       body: Center(
//         child: Padding(
//           padding: const EdgeInsets.all(8.0),
//           child: Column(
//             mainAxisAlignment: MainAxisAlignment.center,
//             children: <Widget>[
//               TextFormField(controller: _controller),
//             ],
//           ),
//         ),
//       ),
//       floatingActionButton: FloatingActionButton(
//         onPressed: () => _connect(),
//         tooltip: 'Cpnnect',
//         child: Icon(Icons.settings_ethernet),
//       ), // This trailing comma makes auto-formatting nicer for build methods.
//     );
//   }
// }

// class PrefsService {
//   static void setString(String key, String item) async {
//     final prefs = await SharedPreferences.getInstance();
//     prefs.setString(key, item);
//   }

//   static Future<String> getString(String key) async {
//     final prefs = await SharedPreferences.getInstance();
//     return prefs.getString(key) ?? '';
//   }
// }

// class ConnectionService {
//   late String _ip;

//   void setIP(String ip) => this._ip = ip;
//   String get ip => this._ip;

//   bool get isConnected => Meteor.isConnected;

//   Future<ConnectionStatus> connect() async {
//     if (isConnected) disconnect();
//     return await Meteor.connect('ws://${this._ip}/websocket',
//         enableLogs: false);
//   }

//   void disconnect() {
//     Meteor.disconnect();
//   }
// }
